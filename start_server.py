import os
import MySQLdb
import datetime
import threading
from time import sleep


table = "load_page_loadimg"


class MyDbWork():
    def __init__(self):
        self.host = 'localhost'
        self.user = 'user'
        self.password = 'pass'
        self.db = 'database'

    def execute_query(self, query, with_return=False):
        try:
            data = None
            con = MySQLdb.connect(
                db=self.db,
                user=self.user,
                password=self.password,
                host=self.host
            )
            with con:
                cursor = con.cursor()
                cursor.execute(query)
                if with_return:
                    data = cursor.fetchall()
                con.commit()
            return data
        except Exception as e:
            print(f"Can't execute query: {query}\nException: {e}")


def watch_for_photos_date():
    db_obj = MyDbWork()
    while True:
        cur_time = datetime.datetime.now().timestamp()
        check_query = f"SELECT photo_hash FROM {table} "\
            f"WHERE time_delete<={cur_time};"
        photo_hashes = db_obj.execute_query(check_query, True)
        if photo_hashes is not None:
            for photo_hash in photo_hashes:
                delete_query = f"DELETE FROM {table} "\
                    f"WHERE photo_hash='{photo_hash[0]}';"
                db_obj.execute_query(delete_query)
                print(f"hash: {photo_hash[0]} was deleted.")
        sleep(60)


thread_watch_for_clean = threading.Thread(target=watch_for_photos_date)
thread_watch_for_clean.start()

os.system("./manage.py runserver")
