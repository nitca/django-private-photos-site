import datetime
import uuid
from django.shortcuts import render, redirect
from .models import *
from django.views.generic import CreateView
from .forms import *


def get_unique_hash():
    """Check for unique photo hash and get it for photos links"""
    p_hash = str(uuid.uuid1().hex)
    while LoadImg.objects.filter(photo_hash=p_hash).exists():
        p_hash = str(uuid.uuid1().hex)

    return p_hash


def get_delete_time(time):
    """Create a photo's delete time. Plus user choice"""
    time = int(time) * 60
    now = datetime.datetime.now().timestamp()
    now += time

    return now


def index(request, p_hash=None):
    main_menu = [
        {'title': 'Поддержать автора', 'url_name': 'donate'},
        {'title': 'Вопрос ответ', 'url_name': 'faq'},
        {'title': 'Контакты', 'url_name': 'contacts'},
    ]
    add_photo_form = AddPhoto()
    get_photo_form = GetPhoto()
    title = "Главная страница"
    context = {
        'add_photo_form': add_photo_form,
        'get_photo_form': get_photo_form,
        'menu': main_menu,
    }
    return render(request, 'load_page/index.html', context)


def load_photo(request):
    """Load or get user photo by hash"""
    if request.method == 'POST' and request.FILES:
        time_delete = request.POST['time_delete']
        new_time = get_delete_time(time_delete)
        photo = request.FILES['photo']
        p_hash = get_unique_hash()
        LoadImg.objects.create(
            photo=photo, time_delete=new_time, photo_hash=p_hash
        )
        return redirect('get_photo', p_hash)


def get_photo(request, p_hash=None):
    if request.is_secure():
        url = "https://"
    else:
        url = "http://"

    url = url + request.get_host() + request.get_full_path()

    if request.method == "POST":
        p_hash = request.POST['photo_hash']
        url = f"{url}{p_hash}"
    p_hash_data = {'url': url, 'p_hash': p_hash}
    user_photo = LoadImg.objects.get(photo_hash=p_hash).photo
    main_menu = [
        {'title': 'Главная', 'url_name': 'home'},
        {'title': 'Вопрос ответ', 'url_name': 'faq'},
        {'title': 'Поддержать автора', 'url_name': 'donate'},
    ]
    context = {
        'photo': user_photo,
        'menu': main_menu,
        'hash_data': p_hash_data
    }
    return render(request, 'load_page/get_photo.html', context)


def donate(request):
    message = {
        'text': "Вы можете поддержать меня донатом, если хотите: ",
        "link": "https://www.donationalerts.com/r/nitca"
    }

    main_menu = [
        {'title': 'Главная', 'url_name': 'home'},
        {'title': 'Вопрос ответ', 'url_name': 'faq'},
        {'title': 'Контакты', 'url_name': 'contacts'},
    ]

    context = {
        'title': 'Поддержать автора',
        'menu': main_menu,
        'message': message,
    }
    return render(request, 'load_page/donate.html', context)


def faq(request):
    repo_link = 'https://gitlab.com/DigitalMonroe/'
    main_menu = [
        {'title': 'Главная', 'url_name': 'home'},
        {'title': 'Поддержать автора', 'url_name': 'donate'},
        {'title': 'Контакты', 'url_name': 'contacts'},
    ]
    faq = [
        {
            'question': 'Для чего этот сервис?',
            'answer':
            'Данный сервис позволяет временно делиться '
            'фотографиями или другими изображениями чтобы те '
            'отсутствовали в переписках. Например интимными.'
        },
        {
            'question': 'Сколько времени храните фото?',
            'answer':
            'Фото хранятся ровно столько времени, сколько '
            'указано при загрузке, от 5 минут до 1 дня.'
        },
        {
            'question': 'Вы точно удаляете фотографии?',
            'answer':
            'Все загруженные фото удаляются автоматически. '
            'Убедиться в этом можете самолично, исходный код сервиса '
            f'открыт и доступен по адресу: ',
            'link': repo_link,
        },
    ]
    context = {
        'title': 'FAQ',
        'menu': main_menu,
        'faq': faq,
    }
    return render(request, 'load_page/faq.html', context)


def contacts(request):
    socials = [
        {'text': 'Вконтакте', 'link': 'https://vk.com/id589323463'},
        {'text': 'Телеграм', 'link': 'https://t.me/ni_nitca'},
    ]
    main_menu = [
        {'title': 'Главная', 'url_name': 'home'},
        {'title': 'Поддержать автора', 'url_name': 'donate'},
        {'title': 'Вопрос ответ', 'url_name': 'faq'},
    ]
    context = {
        'title': 'Контакты',
        'menu': main_menu,
        'socials': socials,
    }
    return render(request, 'load_page/contacts.html', context)
