from django import forms


class AddPhoto(forms.Form):
    photo = forms.ImageField(
        label="Фото", widget=forms.ClearableFileInput(
            attrs={'class': 'photo'}
            )
        )


class GetPhoto(forms.Form):
    photo_hash = forms.CharField(
        max_length=255, label="Хэш", widget=forms.TextInput(
            attrs={'placeholder': 'СЕКРЕТНАЯ СТРОКА', 'class': 'line'}
            )
        )
